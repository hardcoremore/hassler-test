import { Rocket } from '../../components/rockets/rocket';

import { createReducer, on } from '@ngrx/store';
import { getRocketAction, setRocketAction } from './../actions/singleRocket';
import { LoadingState } from '../loading.state';

export const initialState: LoadingState = {
  isLoading: false,
  data: null,
};

export const selectedRocketReducer = createReducer(
  initialState,
  on(getRocketAction, (state) => {
    return {isLoading: true, data: <Rocket>[]}
  }),
  on(setRocketAction, (state, action) => {
    return {isLoading: false, data: action.rocket}
  })
);

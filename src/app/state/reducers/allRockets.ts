import { Rocket } from '../../components/rockets/rocket';

import { createReducer, on } from '@ngrx/store';
import { getAllRocketsAction, setAllRocketsAction } from './../actions/allRockets';
import { LoadingState } from '../loading.state';

export const initialState: LoadingState = {
  isLoading: false,
  data: <Rocket>[],
};

export const allRocketsReducer = createReducer(
  initialState,
  on(getAllRocketsAction, (state) => {
    return {isLoading: true, data: <Rocket>[]}
  }),
  on(setAllRocketsAction, (state, action) => {
    return {isLoading: false, data: action.rockets}
  })
);

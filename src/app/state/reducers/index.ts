import * as allRockets from './allRockets';
import * as currentRocket from './selectedRocket';

export const reducers = {
  rockets: allRockets.allRocketsReducer,
  selectedRocket: currentRocket.selectedRocketReducer,
}

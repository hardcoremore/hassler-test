import { Rocket } from '../../components/rockets/rocket';
import { createAction, props } from '@ngrx/store';

export const GET_ALL_ROCKETS = '[AllRockets] Retrieve All Rockets';
export const SET_ALL_ROCKETS = '[AllRockets] Set All Rockets';

export const getAllRocketsAction = createAction(
  GET_ALL_ROCKETS,
  props<{ isLoading: boolean }>()
);

export const setAllRocketsAction = createAction(
  SET_ALL_ROCKETS,
  props<{ rockets: Rocket[] }>()
);

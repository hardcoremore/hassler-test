import { Rocket } from '../../components/rockets/rocket';
import { createAction, props } from '@ngrx/store';

export const GET_ROCKET = '[Rocket] Retrieve Rocket';
export const SET_ROCKET = '[Rocket] Set Rocket';

export const getRocketAction = createAction(
  GET_ROCKET,
  props<{ isLoading: boolean }>()
);

export const setRocketAction = createAction(
  SET_ROCKET,
  props<{ rocket: Rocket }>()
);

import { RocketFilter } from './../../components/rockets/rocketFilter';
import { AppState } from './../app.state';
import { LoadingState } from './../loading.state';
import { createSelector } from "@ngrx/store";
import { Rocket } from 'src/app/components/rockets/rocket';
import { Observable } from 'rxjs';

export const areAllRocketsLoadingSelector = createSelector(
  (state: AppState) => state.rockets,
  (rockets: LoadingState) => rockets.isLoading
);

export const allRocketsSelector = createSelector(
  (state: AppState) => state.rockets,
  (rockets: LoadingState, props: RocketFilter) => {
    if(props.isFilterActive) {
      return (rockets.data as Rocket[]).filter((element:Rocket) => element.rocket_name?.indexOf(props.name))
    }
    return rockets.data;
  }
);

export const isSelectedRocketLoadingSelector = createSelector(
  (state: AppState) => state.selectedRocket,
  (selectedRocket: LoadingState) => selectedRocket.isLoading
);

export const selectedRocketSelector = createSelector(
  (state: AppState) => state.selectedRocket,
  (selectedRocket: LoadingState) => {
    return selectedRocket.data
  }
);

import { Rocket } from '../components/rockets/rocket';

export interface AppState {
  selectedRocket: {
    isLoading: false,
    data: null
  };
  rockets: {
    isLoading: false,
    data: Rocket[]
  };
}

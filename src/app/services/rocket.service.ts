import { MessagesService } from './messages.service';
import { Injectable } from '@angular/core';
import { Rocket } from '../components/rockets/rocket';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError, retry } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RocketService {

  private rocketsUrl:String = 'rockets';

  constructor(private messageService:MessagesService, private httpClient:HttpClient) { }

  getRockets(): Observable<Rocket[]> {
    this.messageService.add('Rocket Service: fetched rockets from service');
    return this.httpClient.get<Rocket[]>(environment.apiBaseUrl+this.rocketsUrl).pipe(
      map((rockets) => rockets || [])
    );
  }

  getSingleRocket(rocketId: string): Observable<Rocket> {
    this.messageService.add('Rocket Service: fetched single rocket from service');
    return this.httpClient.get<Rocket>(environment.apiBaseUrl+this.rocketsUrl+'/'+rocketId);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RocketsComponent } from './components/rockets/rockets.component';
import { RocketDetailsComponent } from './components/rocket-details/rocket-details.component';

const routes: Routes = [
  { path: 'rockets/:id', component: RocketDetailsComponent },
  { path: 'rockets', component: RocketsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

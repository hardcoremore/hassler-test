import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hassler-test';

  constructor(public router: Router) {}

  ngOnInit(): void {
    if(window.location.pathname === '/') {
      this.router.navigate(['rockets']);
    }
  }
}

import { setRocketAction, getRocketAction } from './../../state/actions/singleRocket';
import { selectedRocketSelector } from './../../state/selectors/rocket.selectors';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Rocket } from '../rockets/rocket';
import { Store, select } from '@ngrx/store';
import { AppState } from './../../state/app.state';
import { Observable } from 'rxjs';
import { RocketService } from '../../services/rocket.service';
import { ActivatedRoute } from "@angular/router";
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-rocket-details',
  templateUrl: './rocket-details.component.html',
  styleUrls: ['./rocket-details.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class RocketDetailsComponent implements OnInit {

  rocket?:Observable<Rocket> = this.store.pipe(select(selectedRocketSelector));
  isSmallScreen:boolean = this.breakpointObserver.isMatched('(max-width: 600px)');
  colsMult:number = 1;

  constructor(private rocketService: RocketService,
              private store:Store<AppState>,
              private route: ActivatedRoute,
              private breakpointObserver:BreakpointObserver) { }

  ngOnInit(): void {

    var rocketId:string = this.route.snapshot.params.id;

    this.store.select(selectedRocketSelector).subscribe((rocket: Rocket) => {
      if(!rocket && rocketId) {
        this.loadRocket(rocketId);
      }
    });

    this.breakpointObserver
    .observe([Breakpoints.Small, Breakpoints.HandsetPortrait, Breakpoints.Medium, Breakpoints.Large])
    .subscribe((state: BreakpointState) => {
      this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 600px)');
      this.colsMult = this.isSmallScreen ?  2 : 1;
    });
  }

  loadRocket(rocketId:string) {
    this.rocketService
    .getSingleRocket(rocketId)
    .subscribe((rocket) => this.store.dispatch(setRocketAction({rocket})));
  }

  getFirstRocketImage(rocket:Rocket):string {
    return rocket.flickr_images ? rocket.flickr_images[0] : '';
  }

  getAllRocketImagesExceptFirst(rocket:Rocket): string[] {
    return rocket.flickr_images ? rocket.flickr_images.slice(1) : [];
  }
}

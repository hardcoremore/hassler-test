import { RocketFilter } from './rocketFilter';
import { setRocketAction } from './../../state/actions/singleRocket';
import { AppState } from './../../state/app.state';
import { getAllRocketsAction, setAllRocketsAction } from '../../state/actions/allRockets';
import { allRocketsSelector, areAllRocketsLoadingSelector } from './../../state/selectors/rocket.selectors';
import { RocketService } from '../../services/rocket.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Rocket } from './rocket';
import { Store, select } from '@ngrx/store';
import { Observable, combineLatest } from 'rxjs';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatSelectChange } from '@angular/material/select';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
    selector: 'app-rockets',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './rockets.component.html',
    styleUrls: ['./rockets.component.css']
})

export class RocketsComponent implements OnInit {

    rockets?:Observable<Rocket[]>;
    isAllRocketsLoading$:Observable<boolean> = this.store.pipe(select(areAllRocketsLoadingSelector));
    isSmallScreen:boolean = this.breakpointObserver.isMatched('(max-width: 600px)');
    rocketFilter: RocketFilter = new RocketFilter();

    constructor(private rocketService: RocketService,
                private store:Store<AppState>,
                public router: Router,
                private breakpointObserver:BreakpointObserver) {}

    ngOnInit(): void {
      this.rockets = this.store.pipe(select(allRocketsSelector, this.rocketFilter));
      this.breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.HandsetPortrait, Breakpoints.Medium, Breakpoints.Large])
      .subscribe((state: BreakpointState) => {
        this.isSmallScreen = this.breakpointObserver.isMatched('(max-width: 600px)');
      });
      this.getRockets();
    }

    onSelect(rocket: Rocket):void {
      this.store.dispatch(setRocketAction({rocket}));
      this.router.navigate(['rockets', rocket.rocket_id]);
    }

    getRockets() {
      this.rocketService
      .getRockets()
      .subscribe((rockets) => this.store.dispatch(setAllRocketsAction({rockets})));
    }

    getRocketImage(rocket:Rocket): string {
      return rocket.flickr_images ? rocket.flickr_images[0] : '';
    }

    rocketFilterNameUpdate(event: Event) {
      this.rocketFilter.name = (event.target as HTMLInputElement).value;
      this.rocketFilter.isFilterActive = true;
    }

    rocketFilterSortByUpdate(event:MatSelectChange) {
      this.rocketFilter.sortBy = event.value;
      this.rocketFilter.isFilterActive = true;
    }

    rocketFilterActiveUpdate(event: MatCheckboxChange) {
      this.rocketFilter.active = event.checked;
      this.rocketFilter.isFilterActive = true;
    }

    rocketFilterReset() {
      this.rocketFilter.name = '';
      this.rocketFilter.sortBy = '';
      this.rocketFilter.active = false;
      this.rocketFilter.isFilterActive = false;
    }
}

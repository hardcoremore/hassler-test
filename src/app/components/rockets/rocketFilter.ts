export class RocketFilter {
  name: string = '';
  sortBy?: string;
  active?: boolean;
  isFilterActive?: boolean;
}

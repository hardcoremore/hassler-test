export class Rocket {
  id?: number;
  active?: boolean;
  stages?: number;
  boosters?: number;
  cost_per_launch?: number;
  success_rate_pct?: number;
  first_flight?: Date;
  country?: string;
  company?: string;
  flickr_images?: string[];
  wikipedia?: string;
  description?: string;
  rocket_id?: string;
  rocket_name?: string;
  rocket_type?: string;
  height?:any;
  diameter?:any;
  mass?:any;
}

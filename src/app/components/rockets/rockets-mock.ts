import { Rocket } from './rocket';

export const ROCKETS: Rocket[] = [
  {id: 1, name: 'Rocket 1'},
  {id: 2, name: 'Rocket 2'},
  {id: 3, name: 'Rocket 3'},
  {id: 4, name: 'Rocket 4'},
  {id: 5, name: 'Rocket 5'},
  {id: 6, name: 'Rocket 6'},
  {id: 7, name: 'Rocket 7'},
  {id: 9, name: 'Rocket 8'},
  {id: 9, name: 'Rocket 9'},
  {id: 10, name: 'Rocket 10'}
];
